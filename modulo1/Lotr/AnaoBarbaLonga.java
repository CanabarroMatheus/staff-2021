public class AnaoBarbaLonga extends Anao {
    private SortearNumero sortearNumero;

    public AnaoBarbaLonga(String nome) {
        super(nome);
        this.sortearNumero = new DadoD6();
    }

    public void sofrerDano() {
        boolean perdeVida = sortearNumero.sortear() <= 4;

        if(perdeVida) {
            super.sofrerDano();
        }
    }
}
