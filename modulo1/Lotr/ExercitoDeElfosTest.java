import static org.junit.Assert.*;

import org.junit.Test;

import java.util.*;

public class ExercitoDeElfosTest {

    @Test
    public void podeAlistarElfoVerde() {
        Elfo elfoVerde = new ElfoVerde("Green Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }
    
    @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }
    
    @Test
    public void naoPodeAlistarElfoDeLuz() {
        Elfo elfoLuz = new ElfoDeLuz("Light Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoLuz);
        assertFalse(exercito.getElfos().contains(elfoLuz));
    }
    
    @Test
    public void buscarElfosRecemCriadosExistindo() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoNoturno);
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(elfoNoturno)
        );
        assertEquals(esperado, exercito.buscar(Status.RECEM_CRIADO));
    }
    
    @Test
    public void organizarContingente() {
        ExercitoDeElfos exercitoDeElfos = new ExercitoDeElfos();
        for(int i = 0; i < 10; i++) {
            if(i >= 5) {
                exercitoDeElfos.alistar(new ElfoVerde("Elfo Verde " + i));
            } else {
                exercitoDeElfos.alistar(new ElfoNoturno("Elfo Noturno " + i));
            }
        }
        ArrayList<Elfo> exercito = exercitoDeElfos.ordemDeAtaqueIntercalado();
        
        assertTrue(exercito.get(0) instanceof ElfoVerde);
        assertTrue(exercito.get(1) instanceof ElfoNoturno);
    }
    
    @Test
    public void atacarAnaoComNoturnosPorUltimo() {
        ExercitoDeElfos exercitoDeElfos = new ExercitoDeElfos();
        for(int i = 0; i < 10; i++) {
            if(i >= 5) {
                exercitoDeElfos.alistar(new ElfoVerde("Elfo Verde " + i));
            } else {
                exercitoDeElfos.alistar(new ElfoNoturno("Elfo Noturno " + i));
            }
        }
        ArrayList<Elfo> exercito = exercitoDeElfos.noturnosPorUltimo();

        assertTrue(exercito.get(9) instanceof ElfoNoturno);
    }
    
    @Test
    public void atacarAnaoIntercalandoOsAtaques() {
        ExercitoDeElfos exercitoDeElfos = new ExercitoDeElfos();

        for(int i = 0; i < 10; i++) {
            if(i >= 5) {
                exercitoDeElfos.alistar(new ElfoVerde("Elfo Verde " + i));
            } else {
                exercitoDeElfos.alistar(new ElfoNoturno("Elfo Noturno " + i));
            }
        }

        ArrayList<Elfo> exercito = exercitoDeElfos.ordemDeAtaqueIntercalado();

        assertTrue(exercito.get(0) instanceof ElfoVerde);
        assertTrue(exercito.get(1) instanceof ElfoNoturno);
    }
}

